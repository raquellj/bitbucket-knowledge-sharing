/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

/**
 *
 * @author EMashishi
 */
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import java.text.SimpleDateFormat;
import java.util.Date;

@KeywordAnnotation(
        Keyword = "Injury Claim",
        createNewBrowserInstance = false
)
public class InjuryClaim extends BaseClass {

    String error = "";

    public InjuryClaim() {
    }

    public TestResult executeTest() {

        if (!claimForm()) {
            narrator.testFailed("Due to -" + error);
        }

        if (!IncidentWorker()) {
            narrator.testFailed("Due to -" + error);
        }

        if (!workerEmploymentDetails()) {
            narrator.testFailed("Due to -" + error);
        }

        if (!workerEarning()) {
            narrator.testFailed("Due to -" + error);
        }

        if (!treatmentReturnToWorkDetails()) {
            narrator.testFailed("Due to -" + error);
        }

        if (!employerLodgementDetails()) {
            narrator.testFailed("Due to -" + error);
        }

        return narrator.finalizeTest("Successfully Inprogress Audit");
    }

    public boolean claimForm() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryClaimTab())) {
            error = "Failed to click injury Claim Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryClaimAdd())) {
            error = "Failed to click injury Claim add ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        String[] unit = getData("Business unit").split(",");

        for (int k = 0; k < unit.length; k++) {

            if (k + 1 == unit.length) {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(unit[k]))) {
                    error = "Failed to click asset - " + unit[k];
                    return false;
                }
                narrator.stepPassed("SuccessFully asset : " + unit[k]);
            } else {

                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveDropdownXpath(unit[k]))) {
                    error = "Failed to click asset - " + unit[k];
                    return false;
                }
                narrator.stepPassed("SuccessFully asset : " + unit[k]);
            }

        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.specificLoca(), getData("Specific location"))) {
            error = "Failed to enter injury Claim - Specific location ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workersTabXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employeeDropdownXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details - Employee dropdown ";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.clickElementsbyXpath(InjuryClaimPageObject.anySupervisorXpath(getData("Full Name")), 1)) {
            error = "Failed to click injury Claim - Worker's Personal Details - Employee  " + getData("Full Name");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.languageDropdownXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details - language dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(getData("language")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - language  " + getData("language");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.specialCommunicationXpath(), getData("special communication"))) {
            error = "Failed to enter injury Claim - Worker's Personal Details - special communication ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.compensationClaimDropdownXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details - compensation Claim dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(getData("compensation claim")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - compensation claim  " + getData("compensation claim");
            return false;
        }

        if (getData("compensation claim").equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.partnerDropDownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(getData("partner")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + getData("Do you support a partner?");
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.averageGrossXpath(), getData("average gross"))) {
                error = "Failed to click injury Claim - Worker's Personal Details - average gross " + getData("average gross");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.fullTimeStudentsXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(getData("full-time students?")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students  " + getData("full-time students?");
                return false;
            }

            narrator.stepPassedWithScreenShot("Suceesfully entered Worker's Personal Details");

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workersTabXpath())) {
                error = "Failed to close injury Claim Tab ";
                return false;
            }

        }

        return true;
    }

    public boolean IncidentWorker() {

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabXpath())) {
            error = "Failed to wait for Incident and Worker's Injury Details Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.IncidentTabXpath())) {
            error = "Failed to click Incident and Worker's Injury Details Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("condition")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - condition";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("condition")))) {
            error = "Failed to click Incident and Worker's Injury Details - condition";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.whatHappenedXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.whatHappenedXpath(), getData("what happened"))) {
            error = "Failed to enter - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.youWereInjuredXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.youWereInjuredXpath(), getData("you were injured"))) {
            error = "Failed to enter - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.responsibleEmployerXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.responsibleEmployerXpath(), getData("Responsible employer"))) {
            error = "Failed to enter - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to click Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.accidentWasReportedXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.accidentWasReportedXpath(), getData("accident was reported to"))) {
            error = "Failed to enter - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.involvedVehiclesXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - involved vehiclese  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.involvedVehiclesXpath(), getData("involved vehicles"))) {
            error = "Failed to enter - Incident and Worker's Injury - involved vehiclese  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.StateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - State  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.StateXpath(), getData("State"))) {
            error = "Failed to enter - Incident and Worker's Injury - State  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryCausesDropdownXpath())) {
            error = "Failed to wait for Incident and Worker's Injury Details - Do you believe that your injury dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryCausesDropdownXpath())) {
            error = "Failed to click Incident and Worker's Injury Details - Do you believe that your injury dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(getData("Injury Causes")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - Do you believe that your injury";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(getData("Injury Causes")))) {
            error = "Failed to click Incident and Worker's Injury Details - Do you believe that your injury";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Third party")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - Third party " + getData("Third party");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Third party")))) {
            error = "Failed to click Incident and Worker's Injury Details - Third party " + getData("Third party");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.additionalDescriptionXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - Additional description  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.additionalDescriptionXpath(), getData("Additional description"))) {
            error = "Failed to enter - Incident and Worker's Injury - Additional description  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What was the date the injury  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryDateXpath(), startDate)) {
            error = "Failed to enter - Incident and Worker's Injury - What was the date the injury  ";
            return false;
        }
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryTimeXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What was the Time the injury  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryTimeXpath(), time)) {
            error = "Failed to enter - Incident and Worker's Injury - What was the Time the injury  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryConditionsXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryConditionsXpath(), getData("Injury Conditions"))) {
            error = "Failed to enter - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.stoppedWorkingDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stoppedWorkingDateXpath(), startDate)) {
            error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.stoppedWorkingTimeXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stoppedWorkingTimeXpath(), time)) {
            error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.reportDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.reportDateXpath(), startDate)) {
            error = "Failed to enter - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Injury reported to")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Injury reported to")))) {
            error = "Failed to click Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
            error = "Failed to wait for Incident and Worker's Injury Details - Have you previously had another injury ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
            error = "Failed to click Incident and Worker's Injury Details - Have you previously had another injury ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Preview Injury")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - TInjury reported to " + getData("Preview Injury");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Preview Injury")))) {
            error = "Failed to click Incident and Worker's Injury Details - TInjury reported to " + getData("Preview Injury");
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully entered Incident and Worker's Injury Details");

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabXpath())) {
            error = "Failed to wait for Close Incident and Worker's Injury Details Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.IncidentTabXpath())) {
            error = "Failed to Close Incident and Worker's Injury Details Tab ";
            return false;
        }

        return true;
    }

    public boolean workerEmploymentDetails() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workerEmploymentDetailsTab())) {
            error = "Failed to click Worker's Employment Details  Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameOrgPayingInjuredXpath(), getData("Name Org paying injured"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of organisation paying your wages when you were injured ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.workPlaceAddressXpath(), getData("Workplace Address"))) {
            error = "Failed to enter - Worker's Employment Details  - Street address of your usual workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.suburbXpath(), getData("Suburb"))) {
            error = "Failed to enter - Worker's Employment Details  - Suburb ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stateXpath(), getData("Address state"))) {
            error = "Failed to enter - Worker's Employment Details  - State ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.postCodeXpath(), getData("Postcode"))) {
            error = "Failed to enter - Worker's Employment Details  - Postcode ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameofEmployerXpath(), getData("Name of Employer"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.NumOfEmployerXpath(), getData("Number of Employer"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.usualOccupationXpath(), getData("Usual occupation"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("apply to you")))) {
            error = "Failed to click Worker's Employment Details - Which of the following incident circumstances apply? " + getData("apply to you");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.startedWorkDateXpath(), getData("Start work date"))) {
            error = "Failed to enter - Worker's Employment Details - When did you start working for this employer? ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyContainsDropDownCheckXpath(getData("Indicate if any apply to you")))) {
            error = "Failed to click - Worker's Employment Details - Please indicate if any of the following apply to you  " + getData("Indicate if any apply to you");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
            error = "Failed to enter - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Employment at the time injured")))) {
            error = "Failed to click - Worker's Employment Details - Did you have any other employment at the time you were injured?  " + getData("Employment at the time injured");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.addDetailsXpath(), getData("Add Details"))) {
            error = "Failed to enter - Worker's Employment Details - Additional details ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully entered Worker's Employment Details");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workerEmploymentDetailsTab())) {
            error = "Failed to Close Worker's Employment Details Tab ";
            return false;
        }

        return true;
    }

    public boolean workerEarning() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workerEmploymentDetailsTab())) {
            error = "Failed to click Worker's Primary Earning Details  Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.hoursWorkedXpath(), getData("hour worked"))) {
            error = "Failed to enter - Worker's Primary Earning Details   - How many standard hours did you work each week before being injured?";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.workingTimeFromXpath(), getData("Working time from"))) {
            error = "Failed to enter - Worker's Primary Earning Details  - Working time from  ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.workingTimeToXpath(), getData("Working time to"))) {
            error = "Failed to enter - Worker's Primary Earning Details - Working time to ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.weeklyShiftXpath(), getData("Weekly shift allowance"))) {
            error = "Failed to enter - Worker's Primary Earning Details - Weekly shift allowance ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.preTaxRateXpath(), getData("pre-tax rate?"))) {
            error = "Failed to enter - Worker's Primary Earning Details - pre-tax rate? ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.overtimeAlloXpath(), getData("overtime allowance"))) {
            error = "Failed to enter - Worker's Primary Earning Details  - overtime allowance";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.preTaxRateWeeklyXpath(), getData("pre-tax rate earning"))) {
            error = "Failed to enter - Worker's Employment Details  - pre-tax rate earning ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.overtimeWorkedXpath(), getData("overtime worked"))) {
            error = "Failed to enter - Worker's Employment Details  - overtime worked ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully entered Worker's Employment Details");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workerEarningTab())) {
            error = "Failed to Close Worker's Employment Details Tab ";
            return false;
        }

        return true;
    }

    public boolean treatmentReturnToWorkDetails() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
            error = "Failed to click Treatment and Return To Work Details   Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.doctorTreatmentXpath(), getData("nominated treating doctor"))) {
            error = "Failed to enter - Treatment and Return To Work Details - Who is your nominated treating doctor?";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.numOfTreatmentXpath(), getData("Number of nominated treating doctor"))) {
            error = "Failed to enter - Treatment and Return To Work Details - Number of your nominated treating doctor   ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameOfClinicXpath(), getData("Place Treated Injury"))) {
            error = "Failed to enter - Treatment and Return To Work Details -  name (including Clinics or Hospitals) that have treated your injury";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.contactDetailsXpath(), getData("Contact Details"))) {
            error = "Failed to enter - Treatment and Return To Work Details - contact details ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.sameEmployerDropdownXpath())) {
            error = "Failed to enter - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("returned to your same employer")))) {
            error = "Failed to click Treatment and Return To Work Details - returned to your same employer " + getData("returned to your same employer");
            return false;
        }

        if (getData("returned to your same employer").equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.returnDateXpath(), startDate)) {
                error = "Failed to enter - Treatment and Return To Work Details - What was the date? ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.dutiesdropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - What duties are you doing? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.dutiesOption(getData("Duties")))) {
                error = "Failed to click Treatment and Return To Work Details - What duties are you doing? " + getData("Duties");
                return false;
            }

        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.returnClaimsDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer this claim form? ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.DateOfMedCertDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer the first medical certificate? ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.formToYouremployerddropdownXpath())) {
            error = "Failed to enter - Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.dutiesOption(getData("Delivery")))) {
            error = "Failed to click Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown" + getData("Delivery");
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully entered Treatment and Return To Work Details ");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
            error = "Failed to Close Treatment and Return To Work Details Tab ";
            return false;
        }

        return true;
    }

    public boolean employerLodgementDetails() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employerLodgementDetailsTab())) {
            error = "Failed to click Employer Lodgement Details Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.firstDateCompletedCliamsFormXpath(), startDate)) {
            error = "Failed to enter - Employer Lodgement Details  - When did the employer first receive the worker's completed claim form?";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.firstDateMedCertXpath(), startDate)) {
            error = "Failed to enter - Employer Lodgement Details  - When did the employer first receive the worker's medical certificate? ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.dateClaimAgentXpath(), startDate)) {
            error = "Failed to enter - Employer Lodgement Details  - Date claim from forwarded to Agent";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.claimCostXpath(), getData("Cost of Claim"))) {
            error = "Failed to enter - Employer Lodgement Details  - contact details ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employerDropdownXpath())) {
            error = "Failed to enter - Employer Lodgement Details  - Employer dropdown ";
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementsbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Lodgement - Employer")), 1)) {
            error = "Failed to click - Employer Lodgement Details  - Employer" + getData("Lodgement - Employer");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.positionXpath(), getData("Position"))) {
            error = "Failed to enter - Employer Lodgement Details - Position? ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.schemeNumberXpath(), getData("Employee scheme registration number"))) {
            error = "Failed to enter - Employer Lodgement Details - Employee scheme registration number ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully entered Employer Lodgement Details ");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employerLodgementDetailsTab())) {
            error = "Failed to Close Employer Lodgement Details Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryCliamFormSave())) {
            error = "Failed to click Injury Claim Form save  ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }
        SeleniumDriverInstance.pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.closeInjuryClaimForm(), 4)) {
            error = "Failed to close Injury Claim Form close";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.closeInjuryClaimForm())) {
            error = "Failed to close Injury Claim Form close";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.waitTableInjuryClaim(), 20)) {
            error = "Failed to save Injury Claim details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Suceesfully saved Employer Lodgement Details ");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.closeInjuredPersonsForm())) {
            error = "Failed to close Injured person ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }
        
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.addDetailsVXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.addDetailsVXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        return true;
    }

}
