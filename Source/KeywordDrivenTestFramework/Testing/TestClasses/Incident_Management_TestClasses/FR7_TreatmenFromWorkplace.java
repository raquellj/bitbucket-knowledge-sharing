/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Treatment away from workplace",
        createNewBrowserInstance = false
)

public class FR7_TreatmenFromWorkplace extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_TreatmenFromWorkplace() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!treatmentWorkplace()) {
            return narrator.testFailed("Failed fill Injured Persons - Follow up required - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Follow up required ");
    }

    public boolean treatmentWorkplace() {

        if (getData("Treatment away from workplace").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.ifTreatmentAwayFromWorkpaceCheckBoxXPath(), 1)) {
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath())) {
                    error = "Failed to click Treatment away from workplace checkbox";
                    return false;
                }
            }
             if (!SeleniumDriverInstance.scrollToElement(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath())) {
                error = "Failed to Display Treatment away from workplace";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath(), 2)) {
                error = "Failed to Display Treatment away from workplace";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully displayed Treatment away from workplace details");

        } else if (getData("Treatment away from workplace").equalsIgnoreCase("false")) {

            if (SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath(), 2)) {
                error = "Failed to Display Treatment away from workplace";
                return false;
            }
            narrator.stepPassedWithScreenShot("Treatment away from workplace no tabs are triggered ");
        }

        return true;
    }

}
