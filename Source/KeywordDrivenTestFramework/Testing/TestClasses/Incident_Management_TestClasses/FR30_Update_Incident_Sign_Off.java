/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Update Incident Sign Off",
        createNewBrowserInstance = false
)
public class FR30_Update_Incident_Sign_Off extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR30_Update_Incident_Sign_Off() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!UpdateIncidentSignOff()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Updating Incident Sign Off - Main Scenario");
    }

    public boolean UpdateIncidentSignOff() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
            error = "Failed to wait for 4. Incident Sign Off Tab..";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to wait for 4. Incident Sign Off Tab..";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to click 4. Incident Sign Off Tab.";
                return false;
            }
        }

        pause(5000);
        narrator.stepPassedWithScreenShot("Updating Incident Sign Off.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Yesxpath())) {
            error = "Failed to wait for Administrator 1 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Yesxpath())) {
            error = "Failed to click Administrator 1 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Yesxpath())) {
            error = "Failed to wait for Administrator 2 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Yesxpath())) {
            error = "Failed to click Administrator 2 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Yesxpath())) {
            error = "Failed to wait for Administrator 3 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Yesxpath())) {
            error = "Failed to click Administrator 3 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Yesxpath())) {
            error = "Failed to wait for Administrator 4 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Yesxpath())) {
            error = "Failed to click Administrator 4 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Yesxpath())) {
            error = "Failed to wait for Administrator 5 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Yesxpath())) {
            error = "Failed to click Administrator 5 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Yesxpath())) {
            error = "Failed to wait for Administrator 6 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Yesxpath())) {
            error = "Failed to click Administrator 6 Yes option.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully selected YES for all the logged users.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to wait for Incident Management Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to click Incident Management Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.handleMask()) {
            error = "Failed to handle Mask.";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentManagementId())) {
            error = "Failed to wait for Incident Management Record number.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(MainScenario_PageObjects.IncidentManagementId())) {
            error = "Failed to scroll to Incident Management Record number.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentInvestigationStatus())) {
            error = "Failed to wait for Incident Status.";
            return false;
        }

        String status = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.IncidentInvestigationStatus());

        if (!status.equalsIgnoreCase("5.Closed")) {
            error = "Failed to validate investigation status 5.Closed with retrieved text: " + status;
            return false;
        } else {
            narrator.stepPassed("Incident Investigation Status: " + status);
        }

        return true;
    }

}
