/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Capture Learnings and Approval Sign Off Alternate Scenario",
        createNewBrowserInstance = false
)
public class FR28_Capture_Learnings_And_Approval_Sign_Off_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR28_Capture_Learnings_And_Approval_Sign_Off_AlternateScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!CaptureLearningsAndApprovalSignOff()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Capturing Learnings and Approval Sign Off - Alternate Scenario");
    }

    public boolean CaptureLearningsAndApprovalSignOff() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
            error = "Failed to wait for Incident Action Save more option dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
                error = "Failed to wait for Incident Action Save more option dropdown.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
                error = "Failed to click Incident Action Save more option dropdown.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveAndClose_Buttonxpath())) {
            error = "Failed to wait for Incident Action Save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveAndClose_Buttonxpath())) {
            error = "Failed to click Incident Action Save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
            error = "Failed to wait for 3.Incident Investigation.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
                error = "Failed to wait for 3.Incident Investigation.";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
                error = "Failed to click 3.Incident Investigation";
                return false;
            }
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath())) {
            error = "Failed to wait for Learning & Approval tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath())) {
            error = "Failed to click Learning & Approval tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.LearningAndApproval_Findings_Panelxpath())) {
            error = "Failed to wait for Investigation Findings close panel.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.LearningAndApproval_Findings_Panelxpath())) {
            error = "Failed to click Investigation Findings close panel.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_learnings_TextFieldxpath())) {
            error = "Failed to wait for Incident Investigation Learnings to be shared... Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_learnings_TextFieldxpath(), testData.getData("Learnings"))) {
            error = "Failed to EnterText into Incident Investigation Learnings to be shared... Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Conclusion_TextFieldxpath())) {
            error = "Failed to wait for Incident Investigation Conclusion Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_Conclusion_TextFieldxpath(), testData.getData("Conclusion"))) {
            error = "Failed to EnterText into Incident Investigation Conclusion Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.LearningAndApproval_declaration_CheckBoxXpath())) {
            error = "Failed to wait for I Have included all required .... checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.LearningAndApproval_declaration_CheckBoxXpath())) {
            error = "Failed to click I Have included all required .... checkbox.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Learning & Approval Declaration checkbox.");
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Comments_TextFieldxpath())) {
            error = "Failed to wait for Incident Investigation Comments Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_Comments_TextFieldxpath(), testData.getData("Comments"))) {
            error = "Failed to EnterText into Incident Investigation Comments Textfield.";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.IncidentManagementId()).split("#");

        narrator.stepPassed("Incident Investigation Record Number: " + Actionid[1]);

        String incidentRec = Actionid[1];
       
        String button = getData("Submit Step 3");

        if (button.equalsIgnoreCase("False")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_SaveAndContinue_Buttonxpath())) {
                error = "Failed to wait for Incident Investigation Save And Continue to step 4 button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_SaveAndContinue_Buttonxpath())) {
                error = "Failed to Click Incident Investigation Save And Continue to step 4 button.";
                return false;
            }
            narrator.stepPassed("Suceessfully clicked Save And Continue to step 4.");

        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Submit_Buttonxpath())) {
                error = "Failed to wait for Incident Investigation Submit step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_Submit_Buttonxpath())) {
                error = "Failed to click Incident Investigation Submit step 3 button.";
                return false;
            }
            narrator.stepPassed("Suceessfully clicked Submit Step 3.");

        }
        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentmanagement_Refresh_Buttonxpath())) {
            error = "Failed to wait for Incident Management Refresh button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentmanagement_Refresh_Buttonxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentmanagement_Refresh_Buttonxpath())) {
                error = "Failed to wait for Incident Management Refresh button.";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentmanagement_Refresh_Buttonxpath())) {
                error = "Failed to click Incident Management Refresh button.";
                return false;
            }
        }

        pause(9000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incident_xpath(incidentRec))) {
            error = "Failed to wait for Incident: " + Actionid;
            return false;
        }

        if (!SeleniumDriverInstance.hoverOverElementByXpath(MainScenario_PageObjects.incident_xpath(incidentRec))) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incident_xpath(incidentRec))) {
                error = "Failed to wait for Incident: " + Actionid;
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.hoverOverElementByXpath(MainScenario_PageObjects.incident_xpath(incidentRec))) {
                error = "Failed to hover over Incident: " + Actionid;
                return false;
            }
        }

        return true;
    }

}
