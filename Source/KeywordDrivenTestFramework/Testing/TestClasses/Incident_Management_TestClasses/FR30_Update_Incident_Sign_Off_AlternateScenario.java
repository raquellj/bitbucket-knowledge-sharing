/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Update Incident Sign Off Alternate Scenario",
        createNewBrowserInstance = false
)
public class FR30_Update_Incident_Sign_Off_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR30_Update_Incident_Sign_Off_AlternateScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!UpdateIncidentSignOff()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Updating Incident Sign Off - Alternate Scenario");
    }

    public boolean UpdateIncidentSignOff() {

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath(), 500)) {
            error = "Failed to wait for 4. Incident Sign Off Tab..";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to wait for 4. Incident Sign Off Tab..";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to click 4. Incident Sign Off Tab.";
                return false;
            }
        }

        pause(5000);
        narrator.stepPassedWithScreenShot("Updating Incident Sign Off.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Noxpath())) {
            error = "Failed to wait for Administrator 1 No option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Noxpath())) {
            error = "Failed to click Administrator 1 No option.";
            return false;
        }
     
        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin1_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 1 Details text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Noxpath())) {
            error = "Failed to wait for Administrator 2 No option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Noxpath())) {
            error = "Failed to click Administrator 2 No option.";
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin2_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 2 Details text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Noxpath())) {
            error = "Failed to wait for Administrator 3 No option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Noxpath())) {
            error = "Failed to click Administrator 3 No option.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin3_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 3 Details text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Noxpath())) {
            error = "Failed to wait for Administrator 4 No option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Noxpath())) {
            error = "Failed to click Administrator 4 No option.";
            return false;
        }
   
        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin4_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 4 Details text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Noxpath())) {
            error = "Failed to wait for Administrator 5 No option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Noxpath())) {
            error = "Failed to click Administrator 5 No option.";
            return false;
        }
 
        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin5_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 5 Details text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }      

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Noxpath())) {
            error = "Failed to click Administrator 6 No option.";
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.IncidentSignOff_Admin6_Detailsxpath(), testData.getData("Details"))){
            error = "Failed to enter text into Administrator 6 Details text box.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully selected NO and entered DETAILS for all the logged users.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to wait for Incident Management Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to click Incident Management Save button.";
            return false;
        }

        return true;
    }

}
