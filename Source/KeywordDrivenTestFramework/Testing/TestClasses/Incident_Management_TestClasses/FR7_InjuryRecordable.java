/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Is this injury recordable",
        createNewBrowserInstance = false
)

public class FR7_InjuryRecordable extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_InjuryRecordable() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!injuryRecordable()) {
            return narrator.testFailed("Failed fill Injured Persons - Is this injury recordable? " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Is this injury recordable? ");
    }

    public boolean injuryRecordable() {

        if (getData("Is this injury recordable").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsThisInjuryRecordableCheckBoxXPath())) {
                error = "Failed to click on the Is This Injury Recordable checkbox ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath(), 2)) {
                error = "Failed to display - Is It Reportable ";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully displayed - Is It Reportable");

        } else if (getData("Is this injury recordable").equalsIgnoreCase("false")) {

            if (SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath(), 2)) {
                error = "Failed to display - Is It Reportable ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Is It Reportable - no tabs are triggered ");
        }

        return true;
    }

}
