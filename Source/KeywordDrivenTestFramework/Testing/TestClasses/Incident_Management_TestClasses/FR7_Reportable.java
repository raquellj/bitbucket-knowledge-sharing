/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Reportable",
        createNewBrowserInstance = false
)

public class FR7_Reportable extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_Reportable() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
    }

    public TestResult executeTest() {
        if (!reportable()) {
            return narrator.testFailed("Failed fill Injured Persons - Is it reportable" + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Is it reportable");
    }

    public boolean reportable() {

        if (getData("Reportable").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath())) {
                error = "Failed to click - Is It Reportable ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.ReportableToDropdownXPath(), 2)) {
                error = "Failed to display the Reportable";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully displayed - Reportable to");

        } else if (getData("Reportable").equalsIgnoreCase("false")) {

           if (SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.ReportableToDropdownXPath(), 2)) {
                error = "display the Reportable to";
                return false;
            }
            narrator.stepPassedWithScreenShot("Reportable to - no tabs are triggered ");
        }

        return true;
    }

}
