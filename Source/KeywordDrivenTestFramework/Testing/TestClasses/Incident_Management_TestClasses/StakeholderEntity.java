/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.StakeholderPageObject;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Stakeholder Entity",
        createNewBrowserInstance = false
)
public class StakeholderEntity extends BaseClass {

    String error = "";
  

    public StakeholderEntity() {

    }

    public TestResult executeTest() {

        if (!stakeholderEntity()) {
            return narrator.testFailed("Failed to stakeholder Entity due - " + error);
        }

        return narrator.finalizeTest("Completed stakeholder entity");
    }

    public boolean stakeholderEntity() {

         entityname = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
       
        if (!SeleniumDriverInstance.enterTextByXpath(StakeholderPageObject.entityNameXapth(), entityname)) {
            error = "Failed to enter entity name";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(StakeholderPageObject.industryDropdownXpath())) {
            error = "Failed to click  industry Dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Industry")))) {
            error = "Failed to click industry " + getData("Industry");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(StakeholderPageObject.entityDescriptionXapth(), getData("Entity description"))) {
            error = "Failed to enter Entity description";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(StakeholderPageObject.relationShipOwenerDropdownXpath())) {
            error = "Failed to click  Relationship owner Dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Relationship owner")))) {
            error = "Failed to click Relationship owner " + getData("Relationship owner");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("Stakeholder categories")))) {
            error = "Failed to click Stakeholder entity - Stakeholder categories " + getData("Stakeholder categories");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("Applicable business units")))) {
            error = "Failed to click Stakeholder entity - Applicable business units " + getData("Applicable business units");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("Applicable impact types")))) {
            error = "Failed to click Stakeholder entity - Applicable impact types " + getData("Applicable impact types");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(StakeholderPageObject.saveToContinueXpath())) {
            error = "Failed to click Stakeholder entity - save to continue ";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        narrator.stepPassed("Enter Stakeholder entity details Successfully ");

        narrator.stepPassedWithScreenShot("Enter Stakeholder entity details Successfully");

        return true;
    }
}
