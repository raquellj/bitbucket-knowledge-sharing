/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "External parties involved",
        createNewBrowserInstance = false
)
public class IncidentExternalPartiesInvolved extends BaseClass {

    String error = "";

    public IncidentExternalPartiesInvolved() {

    }

    public TestResult executeTest() {

        if (!externalPartiesInvolved()) {
            return narrator.testFailed("Failed to External parties involved check box due - " + error);
        }

        return narrator.finalizeTest("Completed External parties involved check box");
    }

    public boolean externalPartiesInvolved() {

        if (getData("External parties").equalsIgnoreCase("True")) {

            SeleniumDriverInstance.switchToDefaultContent();

            //getting the user from the side
            user = SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsIncidentMainScenarioPageObject.userXpath());

            if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath(),10)) {
                error = "Failed to switch to frame ";
                return false;
            }

            if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
                error = "Failed to switch to frame ";
                return false;
            }

            SeleniumDriverInstance.pause(1900);
            
              if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.partInvolvedXpath(),40)) {
                error = "Failed to scroll External parties involved check box";
                return false;
            }
            
            
            if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.partInvolvedXpath())) {
                error = "Failed to scroll External parties involved check box";
                return false;
            }
            
            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.partInvolvedXpath())) {
                error = "Failed to click External parties involved check box";
                return false;
            }

            String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addNewExternalParty())) {
                error = "Failed to click add new external party";
                return false;
            }

            for (String windowHandle : SeleniumDriverInstance.Driver.getWindowHandles()) {
                if (!windowHandle.equals(parentWindow)) {

                    SeleniumDriverInstance.Driver.switchTo().window(windowHandle);
                    SeleniumDriverInstance.switchToDefaultContent();

                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
                        error = "Failed to switch to frame ";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.waitXpath(),50)) {
                        error = "Failed to display new window of external party";
                        return false;
                    }

                    narrator.stepPassedWithScreenShot("External parties field is displayed ");

                    SeleniumDriverInstance.Driver.close();
                    SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

                    //switch to parent window iframe
                    SeleniumDriverInstance.switchToDefaultContent();
                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
                        error = "Failed to switch to frame ";
                        return false;
                    }

                }

            }

        } else if (getData("External parties").equalsIgnoreCase("false")) {

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.externalPartiesChecked(), 1)) {
                if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.partInvolvedXpath())) {
                    error = "Failed to External parties involved check box";
                    return false;
                }
            }

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.exeternalPartiesText(), 1)) {
                error = "Failed to disable External parties";
                return false;
            }

            narrator.stepPassed("Do not tick the External parties checkbox.");

        } else {

            if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.partInvolvedXpath())) {
                error = "Failed to scroll External parties involved check box";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.partInvolvedXpath())) {
                error = "Failed to click External parties involved check box";
                return false;
            }

            String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addNewExternalParty())) {
                error = "Failed to click add new external party";
                return false;
            }

            for (String windowHandle : SeleniumDriverInstance.Driver.getWindowHandles()) {
                if (!windowHandle.equals(parentWindow)) {

                    SeleniumDriverInstance.Driver.switchTo().window(windowHandle);
                    SeleniumDriverInstance.switchToDefaultContent();

                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
                        error = "Failed to switch to frame ";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.waitXpath(), 60)) {
                        error = "Failed to display new window of external party";
                        return false;
                    }

                    StakeholderEntity entity = new StakeholderEntity();

                    entity.stakeholderEntity();

                    SeleniumDriverInstance.Driver.close();
                    SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

                    //switch to parent window iframe
                    SeleniumDriverInstance.switchToDefaultContent();
                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
                        error = "Failed to switch to frame ";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.incidentManagementreload())) {
                        error = "Failed to click External parties refresh items";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.anyClickOptionCheckXpath(entityname))) {
                        error = "Failed to save Stakeholder entity " + entityname;
                        return false;
                    }

                    if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.anyClickOptionCheckXpath(entityname))) {
                        error = "Failed to scroll Stakeholder entity " + entityname;
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(entityname))) {
                        error = "Failed to click Stakeholder entity " + entityname;
                        return false;
                    }

                    narrator.stepPassed("Saved Stakeholder entity " + entityname);

                }

            }

        }

        return true;
    }
}
