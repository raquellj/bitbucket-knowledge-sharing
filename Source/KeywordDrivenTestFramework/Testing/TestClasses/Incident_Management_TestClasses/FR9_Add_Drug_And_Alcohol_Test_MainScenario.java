/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.DrugAndAlcoholPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Add Drug And Alcohol Test Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_Add_Drug_And_Alcohol_Test_MainScenario extends BaseClass
{

    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;
    String recordNumber2;
    

    public FR9_Add_Drug_And_Alcohol_Test_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        LoggingAnIncident loggedAccident = new LoggingAnIncident();
        recordNumber = "";
    }

    public TestResult executeTest()
    {
        if (!fillInInjuryOrIllnessDetails())
        {
            return narrator.testFailed("Failed to fill in Injury or Illness details - " + error);
        }
        if (!captureDetailsAndsaveStep2())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!AddDrugAndAlcoholTest())
        {
            return narrator.testFailed("Failed to  - " + error);
        }
        if (!RefreshAndValidateAdded())
        {
            return narrator.testFailed("Failed to  - " + error);
        }
        return narrator.finalizeTest("Successfully saved the injured person's Drug and Alcohol test");
    }

    public boolean fillInInjuryOrIllnessDetails()
    {

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.jobProfileTextFieldXPath(), testData.getData("Job Profile")))
        {
            error = "Failed to fill in the Job profile: " + testData.getData("Job Profile");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.positionStartDatePickerXPath(), date))
        {
            error = "Failed to fill in the Position start date: " + date;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.usualJobTasksXPath(), testData.getData("Job Task")))
        {
            error = "Failed to fill in the Job task: " + testData.getData("Job Task");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIllnessClassificationXPath()))
        {
            error = "Failed to click the Injury Classification ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.treeElementXPath(testData.getData("Classification"))))
        {
            error = "Failed to select the Injury item: " + testData.getData("Classification");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Treatment"))))
        {
            error = "Failed to select the Medical Treatment Case item: " + testData.getData("Treatment");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.descriptionXPath(), testData.getData("Description")))
        {
            error = "Failed to fill in the Description field: " + testData.getData("Description");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.activityAtTheTimeDropdownXPath()))
        {
            error = "Failed to click the Activity at the time dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Activity"))))
        {
            error = "Failed to select the Activity at the time item: " + testData.getData("Activity");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsDropdownXPath()))
        {
            error = "Failed to click the Body Parts dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "), 500))
        {
            error = "Failed to scroll to the following item: " + testData.getData("Body Part 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " ")))
        {
            error = "Failed to select the following item: " + testData.getData("Body Part 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(testData.getData("Body Part 2") + " ")))
        {
            error = "Failed to select the following item: " + testData.getData("Body Part 2");
            return false;
        }

        SeleniumDriverInstance.pause(500);
        // Select option on Nature Of Injury dropdown by clicking on tree items untill it leads to the final item
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryLabelXPath()))
        {
            error = "Failed to click the nature of injury label";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryDropdownXPath()))
        {
            error = "Failed to click on the Nature of Injury dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Classification"))))
        {
            error = "Failed to select the following item: " + testData.getData("Classification");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 1") + " ")))
        {
            error = "Failed to select the following item: " + testData.getData("Nature 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 2"))))
        {
            error = "Failed to select the following item: " + testData.getData("Nature 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Nature 3"))))
        {
            error = "Failed to select the following item: " + testData.getData("Nature 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.mechanismDropdownXPath()))
        {
            error = "Failed to click on the Mechanism dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Mechanism 1") + " ")))
        {
            error = "Failed to select the following item: " + testData.getData("Mechanism 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Mechanism 2"))))
        {
            error = "Failed to select the following item: " + testData.getData("Mechanism 2");
            return false;
        }

        // Fill in the Follow Up Description text area and click the Follow Up Required checkbox, Follow Up Details textarea appears
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.treatmentProvidedDescriptionXPath(), testData.getData("Follow Up Description")))
        {
            error = "Failed to fill in the Follow Up Description field: " + testData.getData("Follow Up Description");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FollowUpRequiredCheckBoxXPath()))
        {
            error = "Failed to click on the Follow Up Required checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.followUpDetailsXPath()))
        {
            error = "Failed to wait for the Follow Up Details text area  ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.followUpDetailsXPath(), getData("Follow Up Details")))
        {
            error = "Failed to fill in the Follow Up Details text area: " + getData("Follow Up Details");
            return false;
        }

        //Check Additional Treatment Required Checkbox, Treatment away from home checkbox appears
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.AdditionalTreatmentRequiredCheckBoxXPath()))
        {
            error = "Failed to click on the Additional Treatment Required checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath()))
        {
            error = "Failed to wait for the Treatment Away From Home checkbox to appear ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath()))
        {
            error = "Failed to click on the Treatment Away From Home checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatedInEmergencyRoomCheckBoxXPath()))
        {
            error = "Failed to click on the Treated In Emergency Room checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatedInpatientOverNightCheckBoxXPath()))
        {
            error = "Failed to click on the Treated Inpatient Over Night checkbox ";
            return false;
        }

        // Fill in Treatment Facility Details text area
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath(), getData("Treatment Facility Details")))
        {
            error = "Failed to fill in the Treatment Facility Details text area: " + getData("Treatment Facility Details");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsThisInjuryRecordableCheckBoxXPath()))
        {
            error = "Failed to click on the Is This Injury Recordable checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath()))
        {
            error = "Failed to wait for the Is It Reportable checkbox to appear ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath()))
        {
            error = "Failed to click on the Is It Reportable checkbox";
            return false;
        }

        // Reportable to Dropdown appears after checking Is It Reportable dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReportableToDropdownXPath()))
        {
            error = "Failed to click the Reportable to Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Regulatory authority"))))
        {
            error = "Failed to select the Reportable to item: " + testData.getData("Regulatory authority");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.InjuryClaimCheckBoxXPath()))
        {
            error = "Failed to click on the Injury Claim checkbox";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered the Incident Management/Injured Persons form");
        return true;
    }

    public boolean captureDetailsAndsaveStep2()
    {

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SaveAndCloseXPath()))
        {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.scrollToElement(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath()))
        {
            error = "Failed to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath()))
        {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.personInvolvedXpath(), 1))
        {
            error = "Failed to display Persons Involved";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.witnessStatementsXpath(), 1))
        {
            error = "Failed to display witness Statements";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.euipmentInvolvedTabXpath(), 1))
        {
            error = "Failed to display Equipment Involved";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.Safety(), 1))
        {
            error = "Failed to display Safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.EnvironmentXpath(), 1))
        {
            error = "Failed to display Environment tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.QualitytXpath(), 1))
        {
            error = "Failed to display Quality tab";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.railwaySafetyXpath(), 1))
//        {
//            error = "Failed to display Railway Safety tab";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.socialSusXpath(), 1))
        {
            error = "Failed to display Social Sustainability tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.occHygieneXpath(), 1))
        {
            error = "Failed to display Occupational Hygiene tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.complianceXpath(), 1))
        {
            error = "Failed to display Compliance tab";
            return false;
        }
        String recordID = "";
        try
        {
            String[] record = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.getRecordIdXpath())).getText().split("#");
            recordID = record[1];
        } catch (Exception e)
        {
            error = "Failed to get the record ID - " + e;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementsbyXpath(IncidentPageObjects.clickClose(), 0))
        {
            error = "Failed to click incident management close button";
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentRefresh()))
        {
            error = "Failed to click to incident management refresh button";
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(MainScenario_PageObjects.WaitForTableToLoadXPath(), 60))
        {
            error = "Failed to wait for table to load";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(MainScenario_PageObjects.WaitForTableToLoadXPath(), 60))
        {
            error = "Failed to wait for table to load";
            return false;
        }
 
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.recordXpath(recordID), 120))
        {
            error = "Failed to wait for incident management record id : " + recordID;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.recordXpath(recordID)))
        {
            error = "Failed to click on the incident management record id : " + recordID;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementsByXpath(MainScenario_PageObjects.LoadingDataActiveXPath()))
        {
            error = "Failed to wait for the Loading Data icon to dissapear  ";
            return false;
        }
        narrator.stepPassed("Successfully saved record " + recordID);
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath()))
        {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.personInvolvedXpath(), 200))
        {
            error = "Failed to display Persons Involved";
            return false;
        }

        try
        {
            recordNumber = recordID;
        } catch (Exception e)
        {
            error = "Failed to capture the record number - " + e;
            return false;
        }

        return true;
    }

    public boolean CloseAllTabsAndSearchIncident()
    {
        return true;
    }

    public boolean AddDrugAndAlcoholTest()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WaitForItemXPath(getData("Full Name"))))
        {
            error = "Failed to wait for the injured persons data - " + getData("Full Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.WaitForItemXPath(getData("Full Name"))))
        {
            error = "Failed to click on the injured persons data - " + getData("Full Name");
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(MainScenario_PageObjects.LoadingFormXPath(), 80))
        {
            error = "Webside too long to load wait reached the time out - Loading Form";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.drugAlcoholTab()))
        {
            error = "Failed to move to the Drug and Alcohol Tests tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.drugAlcoholTab()))
        {
            error = "Failed to click on the Drug and Alcohol Tests tab";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.DrugAndAlcoholTableXPath()))
        {
            error = "Failed to wait for the table to load";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.ConductDrugAndAlcoholTestButtonXPath()))
        {
            error = "Failed to click on the Conduct Drug And Alcohol test button";
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        try
        {
            ArrayList<String> tabs_windows = new ArrayList<String>(SeleniumDriverInstance.Driver.getWindowHandles());
            SeleniumDriverInstance.Driver.switchTo().window(tabs_windows.get(1));
            System.out.println("Window handle is: " + SeleniumDriverInstance.Driver.getWindowHandle());
        } catch (Exception e)
        {
            error = "Failed to switch to the new tab - " + e;
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(DrugAndAlcoholPageObjects.EmployeeTypeXPath()))
        {
            error = "Failed to wait for the Employee Type dropdown list";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(DrugAndAlcoholPageObjects.EmployeeTypeXPath()))
        {
            error = "Failed to click on the Employee Type dropdown list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Employee"))))
        {
            error = "Failed to click on the Employee Type item - " + getData("Employee");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.PersonBeingTestedXPath()))
        {
            error = "Failed to click on the Person Being Tested dropdown list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.PersonBeingTestedXPath()))
        {
            error = "Failed to click on the Person Being Tested dropdown list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Full Name"))))
        {
            error = "Failed to click on the Person Being Tested item - " + getData("Full Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.LinkedIncidentXPath()))
        {
            error = "Failed to click on the Linked Incident checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.LinkedIncidentDropdownXPath()))
        {
            error = "Failed to click on the Linked Incident dropdown";
            return false;
        }

        String linkedIncident = "#" + recordNumber + ": " + getData("Incident title");
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(linkedIncident))){
            error = "Failed to wait for the following item: " + linkedIncident;
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(linkedIncident), 100))
        {
            error = "Failed to move to the following item: " + linkedIncident;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(linkedIncident)))
        {
            error = "Failed to select the following item: " + linkedIncident;
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.MedicalTestDateXPath(), date))
        {
            error = "Failed to enter the date in the Medical test date field " + date;
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.ENTER);
        SeleniumDriverInstance.pause(500);
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.PersonSubmitToATestXPath()))
        {
            error = "Failed to click on the Person submit to a test dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Submit a test"))))
        {
            error = "Failed to select the following item: " + getData("Submit a test");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.DrugTestResultXPath()))
        {
            error = "Failed to click on the Drug test result dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Drug test result"))))
        {
            error = "Failed to select the following item: " + getData("Drug test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.AlcoholTestResultXPath()))
        {
            error = "Failed to click on the Alcohol test result dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Alcohol test result"))))
        {
            error = "Failed to select the following item: " + getData("Alcohol test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SaveDrugAndAlcoholTestXPath()))
        {
            error = "Failed to click the Save button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
        {
            error = "Failed to click on the Save button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassed("Successfully saved the injured Persons record: " + text);
            }
        }
        String retrivedRecordNumber2 = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordNumberXPath());
        narrator.stepPassed("Successfully retrieved and stored text: "+retrivedRecordNumber2);
        recordNumber2 = retrivedRecordNumber2.substring(10);
        
        SeleniumDriverInstance.Driver.close();
        
        narrator.stepPassedWithScreenShot("Successfully added a drug and alcohol test for the matching injured person");
        return true;
    }

    public boolean RefreshAndValidateAdded()
    {
        SeleniumDriverInstance.pause(1000);
        try
        {
            ArrayList<String> tabs_windows = new ArrayList<String>(SeleniumDriverInstance.Driver.getWindowHandles());
            SeleniumDriverInstance.Driver.switchTo().window(tabs_windows.get(0));
            System.out.println("Window handle is: " + SeleniumDriverInstance.Driver.getWindowHandle());
        } catch (Exception e)
        {
            error = "Failed to switch to the default tab - " + e;
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RefreshAlcoholAndDrugsXPath()))
        {
            error = "Failed to wait for the refresh button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RefreshAlcoholAndDrugsXPath()))
        {
            error = "Failed to click on the refresh button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.DrugAndAlcoholTableXPath()))
        {
            error = "Failed to wait for the table to load";
            return false;
        }
                
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.PersonBeingTestedTableXPath(recordNumber2)))
        {
            error = "Failed to wait for record #"+ recordNumber2;
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.PersonBeingTestedTableXPath(recordNumber2)))
        {
            error = "Failed to click record: "+ recordNumber2;
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.OpenedRecordNumberXPath(recordNumber2)))
        {
            error = "Failed to wait for title Record #"+ recordNumber2;
            return false;
        }
        pause(5000);
               
        narrator.stepPassedWithScreenShot("Successfully validated Record #"+recordNumber2+", that the Drug And Alcohol test have been added");
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveAndContinueButton_DropDownXPath()))
        {
            error = "Failed to click on the Save And Continue dropdown button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveAndCloseButtonXPath()))
        {
            error = "Failed to click on the Save And Close button";
            return false;
        }
        return true;
    }
    public boolean SaveAndContinue()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveAndContinueButtonXPath()))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassed("Successfully saved the injured Persons record: " + text);
            }
        }
        narrator.stepPassedWithScreenShot("Successfully saved the Drug And Alcohol test");
        return true;
    }
}
