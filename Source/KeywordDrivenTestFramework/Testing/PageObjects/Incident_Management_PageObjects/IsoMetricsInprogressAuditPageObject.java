/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author EMashishi
 */
public class IsoMetricsInprogressAuditPageObject {
    
    
    
    //new
    
    public static String ISOXpath(){
        return "//li[@id='fbc730db-2fd9-4535-a15e-12fbf5a14faf']/i";
    
    }
    
    public static String saveWait(){
        return "//div[@id='form_493EF828-C4E6-4204-9AEC-3ADF6E0501C0']//div[@class='ui inverted dimmer'][@id='divWait']";
    }
    
    public static String performanceEvXpath(){
        return "//li[@id='ad71f5a0-2894-4a3e-9a8c-9bca14cb41d2']/i";
    
    }
    
    public static String verifyOrXpath(){
        return "//li[@id='73e34cd8-9ef5-47a2-a729-b49de2fb7943']/a";
    
    }
    
    public static String actionFeedBackXpath(){
        return "//li[@id='tab_99358277-8A7C-40A1-9ED3-7613E44800C2']";
    }
    
   
    
    public static String auditElememtText(){
        return "//div[@parent='7F8E3330-7144-41AE-95D9-98BB09E78F84']//li[@id='73e34cd8-9ef5-47a2-a729-b49de2fb7943']";
    }
    
    public static String saveActionFeedbackXpath(){
        return "//div[@id='btnSave_form_48B03F54-B0C3-46EA-82D3-1550972DFE59']";
    }
    
    public static String findingOwnerTextXpath(){
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027']//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']";
    }
    
    public static String hazardTextXpath(){
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027']//li[@id='379f59c0-0bfb-441d-b17f-2278200bdfda']";
    }
    
    public static String findingClassificationTextXpath(){
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027']//li[@id='ca7db1c7-d795-4195-8166-70c9d13173c2']";
    }
    
    public static String rootCauseTextXpath(){
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027']//li[@id='2b4e5d1b-50ec-4cc4-b44d-2e6ea9a24d2a']";
    }
    
    public static String adminXpath(){
        return "//a[text()='1 Administrator']";
    }
    
    public static String UserAdmim(){
        return "//li[@id ='b8ad5f90-d582-46c4-b186-d99649824acd']//i[@class='jstree-icon jstree-checkbox']";
    } 
    
    public static String saveActionDetails(){
        return "//div[@id ='btnSave_form_493EF828-C4E6-4204-9AEC-3ADF6E0501C0']";
    }
    
    public static String saveFindingDetialsXpath(){
        return "//div[@id ='btnSave_form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']";
    }
    
    public static String managerXpath(){
        return "//a[text()='2 Manager']";
    }
    
    
    
    public static String findingButtonXpath() {
        return "//li[@id='tab_64706B3F-76E6-43B1-BB38-D56BA68DD73E']";
    }

    public static String strtAuditCheckBoxXpath() {
        return "//div[@parent='64706B3F-76E6-43B1-BB38-D56BA68DD73E'][2]";
    }

    public static String addButtomXpath() {
        return "//div[text()='Audit Findings']//..//div[@id='btnAddNew']";
    }

    public static String auditElementSDown() {
        return "//div[@parent='7F8E3330-7144-41AE-95D9-98BB09E78F84'][3]";
    }

    public static String iso9001Xpath() {
        return "//div[@id='select3_f1c65fa9']//li[@id='fbc730db-2fd9-4535-a15e-12fbf5a14faf']//i[@class='jstree-icon jstree-ocl']";
    }

    public static String decriptionXpath() {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String findingOwnerDownXpath() {
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027'][6]//span";
    }
    
   
    

    public static String classChemicalXpath() {
        return "//li[@id='379f59c0-0bfb-441d-b17f-2278200bdfda']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String hazardDownpath() {
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027'][8]//span[@class='select3-chosen']";
    }

    public static String nothing() {
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027'][7]";
    }

    public static String findingClassDownpath() {
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027'][10]//span[@class='select3-chosen']";
    }

    public static String findingClassRecoXpath() {
        return "//a[@id='ca7db1c7-d795-4195-8166-70c9d13173c2_anchor']";
    }
    
     public static String rootCauseDownXpath() {
        return "//div[@parent='95E14398-836C-4843-B45F-1D65A39C9027'][14]//a";
    }

    public static String workplace() {
        return "//li[@id='1b55a53d-07c9-4c29-96f0-186c9da7105a']/i";
    }
    
     public static String inadequateTrainging() {
        return "//li[@id='2570d18a-7e08-4853-8ee2-0bc4ba2df83c']/i";
    }
     
     public static String noTraingingPro() {
        return "//li[@id='79eff7b3-60e5-4b4a-9eee-69379dcae8fd']/i";
    }
     
    public static String newWork() {
        return "//li[@id='2b4e5d1b-50ec-4cc4-b44d-2e6ea9a24d2a']//i[@class='jstree-icon jstree-checkbox']";
    } 
    
    public static String addButtonAuditXpath(){
        return "//div[text()='Audit Finding Actions']//..//div[@i18n='add_new']";
    }
    
    public static String addButtonActionFeedBackXpath(){
        return "//div[text()='Action Feedback']/..//div[@i18n='add_new']";
    }
    
    public static String actionFeedbackTextArea(){
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }
    
    public static String actionCompletedownXpath(){
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }
    
    public static String closeActionFeedBack(){
        return "//div[@id='form_48B03F54-B0C3-46EA-82D3-1550972DFE59']/div/i[@class='close icon cross']";
    }
    
    public static String actionFeedbackXpath(String name){
        return "//a[text()='"+name+"']//..//i[@class='jstree-icon jstree-checkbox']";
    }
    
    public static String actionCompleteXpath(String name){
        return "//div[contains(@class, 'transition visible')]//a[text()='"+name+"']";
    }
    
    public static String actionDescriptionXpath(){
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static  String departmentResponsibleDownXpath(){
        return "//div[@parent='3C419243-7BEA-4DFE-BC44-088F3555618A'][4]";
    }
    
    public static String actionDueDate(){
        return "//div[@parent='3C419243-7BEA-4DFE-BC44-088F3555618A'][8]//input";
    }
    
    public static String replicateCheckboxXpath(){
        return "//div[@parent='3C419243-7BEA-4DFE-BC44-088F3555618A'][15]";
    }
    public static  String responsiblePersonDownXpath(){
        return "//div[@parent='3C419243-7BEA-4DFE-BC44-088F3555618A'][6]";
    }
    
    public static String miningXpath(){
        return "//a[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']";
    }
    
    public static String selectFromDropdow1n(String option) {
            return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }
    
    public static String selectFromDropdown(String option) {
            return "//div[contains(@class, 'transition visible')]//a[text()='" + option + " ']";
    }

}
